
"""
Módulo para scrapear los precios de productos de Eroski
"""

import os
import sys
import requests
from datetime import datetime
from lxml import html
import json


def obtener_precios(urls):
    resultados = []
    for url in urls:
        r = requests.get(url)

        # Parser
        tree = html.fromstring(r.content)

        # Productos
        productos = tree.xpath("//div[@id='productListZone']/div")

        # Filtrar los frescos (no bandejas)
        productos = filter(lambda p: "bandeja" not in p.xpath("div/div[@class='product-description']/div[@class='description-text']/h2[@role='presentation']/a/text()")[0], productos)

        # Tuplas con las descripciones y precios
        precios = map(lambda x:(x.xpath("div/div[@class='product-description']/div[@class='description-text']/h2[@role='presentation']/a/text()")[0].strip().split(",")[0], x.xpath("div/div[@class='product-description']/div[@class='description-text']/div[@class='product-row-100 kilo-evaluation']/*/p[@class='quantity-price']/span/text()")), productos)

        resultados.extend(precios)

    print("Precios obtenidos")

    return resultados


def save(precios):
    output_json = 'precios.json'
    fecha = datetime.now().strftime("%d%m%Y")

    if not os.path.exists(output_json):
        precio_dict = {'num_precios': 0, 'fechas': [], 'productos': {}}
    else:
        with open(output_json, 'r') as f:
            precio_dict = json.load(f)

    if fecha not in precio_dict['fechas']:
        print("Guardando precios")

        # Guardar los precios recogidos
        for nombre, precio in precios:
            if nombre not in precio_dict['productos'].keys():
                precio_dict['productos'][nombre] = ['?' for _ in range(precio_dict['num_precios'])]

            if precio and precio[0].lower().strip() == "1 kilo a":
                precio_dict['productos'][nombre].append(float(precio[1].split(" ")[0].replace(",", ".")))
            else:
                precio_dict['productos'][nombre].append('?')

        precio_dict['num_precios'] += 1
        precio_dict['fechas'].append(fecha)

        # Comprobar que todos los productos tienen el mismo número de precios
        for producto in precio_dict['productos'].keys():
            while len(precio_dict['productos'][producto]) < precio_dict['num_precios']:
                precio_dict['productos'][producto].append('?')

        with open(output_json, 'w') as f:
            json.dump(precio_dict, f, ensure_ascii=True, indent=4)
            print("JSON guardado")
    else:
        print("Los precios de hoy ya han sido guardados!")


def json2csv():
    with open("precios.json", 'r') as f:
        j = json.load(f)

    columnas = list(j['productos'].keys())
    filas = list(j['fechas'])
    csv = "fecha\t"+"\t".join(map(str, columnas)) + "\n"

    for i in range(j['num_precios']):
        csv += "\t".join(map(str, [filas[i]]+[j['productos'][c][i] for c in columnas])) + "\n"

    with open("precios.csv", 'w') as f:
        f.write(csv)

    print("CSV con los precios generado")


if __name__ == '__main__':

    if len(sys.argv[1:]) == 0:
        # Guardar los precios
        print("Obteniendo precios")

        urls = [
            'https://supermercado.eroski.es/es/supermercado/2059698-alimentos-frescos/2059722-pescados/2059723-merluza-lirio-y-pescadilla/',
            'https://supermercado.eroski.es/es/supermercado/2059698-alimentos-frescos/2059722-pescados/2059724-anchoa-sardina-chicharro-y-verdel/'
        ]
        precios = obtener_precios(urls)
        save(precios)
    else:
        # Generar el csv
        print("Generando CSV")
        json2csv()


